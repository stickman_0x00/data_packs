playsound minecraft:block.chain.step block @a -1967 59 -858 2

# bottom
fill -1967 65 -855 -1967 65 -863 minecraft:iron_bars
fill -1967 66 -865 -1967 66 -853 minecraft:iron_bars
fill -1967 67 -866 -1967 67 -852 minecraft:iron_bars
fill -1967 68 -867 -1967 68 -851 minecraft:iron_bars

# top
fill -1967 75 -851 -1967 75 -853 air
fill -1967 75 -864 -1967 75 -867 air
setblock -1967 76 -863 air
setblock -1967 76 -854 air
fill -1967 77 -855 -1967 77 -862 air
setblock -1967 77 -861 minecraft:chain
setblock -1967 77 -856 minecraft:chain

schedule function egypt:water_gate/close/1 0.3s