playsound minecraft:block.chain.step block @a -1967 59 -858 2
# bottom
fill -1967 63 -855 -1967 63 -863 minecraft:iron_bars
fill -1967 64 -865 -1967 64 -853 minecraft:iron_bars
fill -1967 65 -866 -1967 65 -852 minecraft:iron_bars
fill -1967 66 -867 -1967 66 -851 minecraft:iron_bars

# top
fill -1967 73 -851 -1967 73 -853 air
fill -1967 73 -864 -1967 73 -867 air
setblock -1967 74 -863 air
setblock -1967 74 -854 air
fill -1967 75 -855 -1967 75 -862 air
setblock -1967 75 -861 minecraft:chain
setblock -1967 75 -856 minecraft:chain

schedule function egypt:water_gate/close/3 0.3s