playsound minecraft:block.chain.step block @a -1967 59 -858 2

# bottom
fill -1967 64 -855 -1967 64 -863 minecraft:iron_bars
fill -1967 65 -865 -1967 65 -853 minecraft:iron_bars
fill -1967 66 -866 -1967 66 -852 minecraft:iron_bars
fill -1967 67 -867 -1967 67 -851 minecraft:iron_bars

# top
fill -1967 74 -851 -1967 74 -853 air
fill -1967 74 -864 -1967 74 -867 air
setblock -1967 75 -863 air
setblock -1967 75 -854 air
fill -1967 76 -855 -1967 76 -862 air
setblock -1967 76 -861 minecraft:chain
setblock -1967 76 -856 minecraft:chain

schedule function egypt:water_gate/close/2 0.3s