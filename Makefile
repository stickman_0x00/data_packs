8bits_world:
	echo $@
	cd $@; zip -r ../$@.zip *

clean:
	rm -f 8bits_world.zip

.PHONY: 8bits_world